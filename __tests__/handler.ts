import { QRHandler } from "../src/QRHandler";
class TestQRHandler extends QRHandler{
    public validate(_qr: string): boolean {
        throw new Error("Method not implemented.");
    }
    public run(qr: string): void {
        throw new Error("Method not implemented.");
    }

}
describe('qr parser test:', () => {
    it(`should create handler success`, () => {
        const testHandler = new TestQRHandler('test1' )
        expect(testHandler.getName()).toEqual('test1')
    });

    it(`should run handler`, () => {

        const t = () => {
            const testHandler = new TestQRHandler('test2')
            testHandler.run('0x')
        };
        expect(t).toThrow('Method not implemented.'); 
    });
});