import { Action } from "../src/Action";
class TestAction extends Action{
    public validate(..._args: string[]): boolean {
        throw new Error("Method not implemented.");
    }
    public run(..._args: string[]): void {
        throw new Error("Method not implemented.");
    }

}
describe('qr parser test:', () => {
    it(`should create handler success`, () => {
        const testHandler = new TestAction('test1')
        expect(testHandler.getName()).toEqual('test1')
    });

    it(`should run handler`, () => { 
        const t = () => {
            const testHandler = new TestAction('test2')
            testHandler.run('0x')
        };
        expect(t).toThrow('Method not implemented.');
    });

    it(`should run handler`, () => { 
        const t = () => {
            const testHandler = new TestAction('test2')
            testHandler.validate('0x')
        };
        expect(t).toThrow('Method not implemented.');
    });
});