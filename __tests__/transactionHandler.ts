import { AddressHandler } from "../src/handler/AddressHandler";
import { TransactionHandler } from "../src/handler/TransactionHandler";
import { GB } from "../src/parse/GB";
import { QRParser } from "../src/QRParser";

const gbTx1 = GB.stringify({
    name: 'TRANSACTION1',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
        DATA: '0x',
        GAS_PRICE: 21000,
        GAS_LIMIT: 30000,
    }
})

const gbTx2 = GB.stringify({
    name: 'TRANSACTION',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
        GAS_PRICE: 21000,
        GAS_LIMIT: 30000,
    }
})

const gbTx3 = GB.stringify({
    name: 'TRANSACTION',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
        DATA: '0x1',
        GAS_PRICE: 21000,
        GAS_LIMIT: 30000,
    }
})

const GBTx4 = GB.stringify({
    name: 'TRANSACTION1',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
    }
})

const testTx1 = JSON.stringify({
    name: 'TRANSACTION',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
        DATA: '0x',
        GAS_PRICE: 21000,
        GAS_LIMIT: 30000,
    }
})

const testTx2 = JSON.stringify({
    name: 'TRANSACTION',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
        GAS_PRICE: 21000,
        GAS_LIMIT: 30000,
    }
})

const testTx3 = JSON.stringify({
    name: 'TRANSACTION',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
        DATA: '0x1',
        GAS_PRICE: 21000,
        GAS_LIMIT: 30000,
    }
})

const testTx4 = JSON.stringify({
    name: 'TRANSACTION1',
    data: {
        TO: '0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445',
        VALUE: 1,
    }
})


describe('qr parser test:', () => {
    it(`should get current handlers`, () => {
        const parser = new QRParser()
        const addressHandler = new AddressHandler('address', () => { })
        parser.register(addressHandler)
        const txHandler = new TransactionHandler('tx', () => { }, 'json')
        parser.register(txHandler)
        const handlerAddress = parser.getParserHandler('0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445')
        expect(handlerAddress?.getName()).toEqual('address')
        const handler1 = parser.getParserHandler(testTx1)
        expect(handler1?.getName()).toEqual('tx')

        const handler2 = parser.getParserHandler(testTx2)
        expect(handler2?.getName()).toEqual('tx')

        const handler4 = parser.getParserHandler(testTx4)
        expect(handler4).toEqual(undefined)

    });

    it(`can not get address handler success`, () => {
        const parser = new QRParser()
        const txHandler = new TransactionHandler('tx', () => { }, 'json')
        parser.register(txHandler)
        const handler3 = parser.getParserHandler(testTx3)
        expect(handler3).toEqual(undefined)
    });


    it(`should console tx success`, () => {
        const parser = new QRParser()
        const txHandler = new TransactionHandler('tx', () => { }, 'json')
        parser.register(txHandler)
        const handler1 = parser.getParserHandler(testTx1)
        handler1?.run(testTx1)
        expect(handler1?.getName()).toEqual('tx')
    });


    /**
     * 国标test
     */

    it(`should get current handlers`, () => {
        const parser = new QRParser()
        const addressHandler = new AddressHandler('address', () => { })
        parser.register(addressHandler)
        const txHandler = new TransactionHandler('tx', () => { })
        parser.register(txHandler)
        const handlerAddress = parser.getParserHandler('0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445')
        expect(handlerAddress?.getName()).toEqual('address')

        const handler1 = parser.getParserHandler(gbTx1)
        expect(handler1).toEqual(undefined)

        const handler2 = parser.getParserHandler(gbTx2)
        expect(handler2?.getName()).toEqual('tx')

        const handler4 = parser.getParserHandler(GBTx4)
        expect(handler4).toEqual(undefined)

    });

    it(`can not get address handler success`, () => {
        const parser = new QRParser()
        const txHandler = new TransactionHandler('tx', () => { })
        parser.register(txHandler)
        const handler3 = parser.getParserHandler(gbTx3)
        expect(handler3).toEqual(undefined)
    });


});