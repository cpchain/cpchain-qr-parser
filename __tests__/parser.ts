import { QRHandler } from "../src/QRHandler";
import { QRParser } from "../src/QRParser";

class TestQRHandler extends QRHandler{
    public validate(_qr: string): boolean {
        throw new Error("Method not implemented.");
    }
    public run(qr: string): void {
        throw new Error("Method not implemented.");
    }

}
describe('qr parser test:', () => {
    it(`should register handler success`, () => {
        const parser = new QRParser()
        const testHandler = new TestQRHandler('test1')
        parser.register(testHandler)
        const handlers = parser.getHandlers()
        expect(handlers.size).toEqual(1)
    });

    it(`should unregister handler`, () => {
        const parser = new QRParser()
        const testHandler = new TestQRHandler('test2')
        parser.register(testHandler)
        parser.unregister(testHandler)
        const handlers = parser.getHandlers()
        expect(handlers.size).toEqual(0)
    });

    it(`should register handler failed and throw error:'can not get name from handler'`, () => {
        const t = () => {
            const parser = new QRParser()
            const testHandler: any = {}
            parser.register(testHandler)
        };
        expect(t).toThrow('can not get name from handler');
    });

    it(`should register handler failed and throw error:'handler do not have a validate'`, () => {
        const t = () => {
            const parser = new QRParser()
            const testHandler: any = {
                getName: () => {
                    return 'test4'
                }
            }
            parser.register(testHandler)
        };
        expect(t).toThrow('handler do not have a validate');
    });

    it(`should not get an handler`, () => {
        const parser = new QRParser()
        const testHandler: any = {
            getName: () => {
                return 'test5'
            },
            validate: () => {
                return true
            }
        }
        const falseHandler: any = {
            getName: () => {
                return 'test6'
            },
            validate: () => {
                return false
            }
        }
        parser.register(testHandler)
        parser.register(falseHandler)
        const handler = parser.getParserHandler('')
        expect(handler?.getName() === 'test5')
    });

    it(`should not get a handler`, () => {
        const parser = new QRParser()
        const handler = parser.getParserHandler('0x')
        expect(handler === undefined)
    });

    it(`should throw error`, () => {
        const t = () => {
            const parser = new QRParser()
            const testHandler = new TestQRHandler('test2')
            parser.register(testHandler)
            parser.getParserHandler('0x')
        };
        expect(t).toThrow('Method not implemented.');
    });
});