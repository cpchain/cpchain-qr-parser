import { AddressHandler } from "../src/handler/AddressHandler";
import { QRParser } from "../src/QRParser";

describe('qr parser test:', () => {
    it(`should get address handler success`, () => {
        const parser = new QRParser()
        const addressHandler = new AddressHandler('address', () => { })
        parser.register(addressHandler)
        const handler = parser.getParserHandler('0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445')
        expect(handler?.getName()).toEqual('address')
    });



    it(`can not get address handler success`, () => {
        const parser = new QRParser()
        const addressHandler = new AddressHandler('address', () => { })
        parser.register(addressHandler)
        const handler = parser.getParserHandler('0xD92')
        expect(handler).toEqual(undefined)
    });


    it(`should console address success`, () => {
        const parser = new QRParser()
        const addressHandler = new AddressHandler('address', (address:string) => {console.log(address) })
        parser.register(addressHandler)
        const handler = parser.getParserHandler('0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445')
        handler?.run('0xD92DA0135fC65F6ceAA2CC4Ad6639e20239e9445')
        expect(handler?.getName()).toEqual('address')
    });

});