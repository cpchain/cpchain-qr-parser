import { ApplicationProps } from "../src/handler/ApplicationHandler";
import { ScanData } from "../src/interface/scan";
import { GB } from "../src/parse/GB";

const testApplicationData = `APPLICATION:
APP_NAME:Identity
ACTION:register
ARGS:"identityName"
END`

describe('qr parser test:', () => {
    it(`should parse data`, () => {
        const { name, data }: ScanData<ApplicationProps> = GB.parse(testApplicationData)
        expect(name).toEqual('APPLICATION')
        expect(data.APP_NAME).toEqual('Identity') 
        const str = GB.stringify({name,data})
        expect(str).toEqual(testApplicationData)
    }); 

});