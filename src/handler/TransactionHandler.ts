import assert from "assert";
import { ethers } from "ethers";
import { ScanData } from "../interface/scan";
import { GB } from "../parse/GB";
import { QRHandler } from "../QRHandler";

export interface TXProps {
    TO: string
    VALUE: number | string
    DATA: string
    GAS_LIMIT: number | string
    GAS_PRICE: number | string
}

export class TransactionHandler extends QRHandler {
    private _parse: (qr: string) => any
    constructor(_name: string, private _callback: Function, parseType?: string) {
        super(_name)
        this._parse = (parseType || 'gb') === 'json' ? JSON.parse : GB.parse
    }
    public validate(qr: string): boolean {
        try {
            const { name, data }: ScanData<TXProps> = this._parse(qr)
            if (name != 'TRANSACTION') return false
            // 判断地址是否有效
            assert.ok(ethers.utils.isAddress(data.TO))
            // 如果携带date，视为合约交易    
            assert(!data.DATA || ethers.utils.hexValue(data.DATA) == '0x0')

            return true
        } catch (_e) {
            return false;
        }
    }

    public run(qr: string): void {
        try {
            const scanData = JSON.parse(qr)
            this._callback(scanData)
        } catch (_e) {
        }
    }
}