import { ethers } from "ethers";
import { QRHandler } from "../QRHandler";

export class AddressHandler extends QRHandler {
    constructor(_name: string, private _callback: Function) {
        super(_name)
    }
    public validate(qr: string): boolean {
        return ethers.utils.isAddress(qr)
    }

    public run(qr: string) {
        this._callback(qr)
    }
}