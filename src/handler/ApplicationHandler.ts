import assert from "assert";
import { Application } from "../Application";
import { ScanData } from "../interface/scan";
import { GB } from "../parse/GB";
import { QRHandler } from "../QRHandler";

export interface ApplicationProps {
    APP_NAME: string
    ACTION: string
    ARGS: string[]
}

export class ApplicationHandler extends QRHandler {
    private _applications: Map<string, Application> = new Map()
    private _parse: (qr: string) => any
    constructor(_name: string, parseType?: string) {
        super(_name)
        this._parse = (parseType || 'gb') === 'json' ? JSON.parse : GB.parse
    }
    public validate(qr: string): boolean {
        try {
            const { name, data }: ScanData<ApplicationProps> = this._parse(qr)
            if (name != 'APPLICATION') return false
            assert.ok(data.APP_NAME && this._applications.has(data.APP_NAME))
            // 校验参数是否合法 
            return this._applications.get(data.APP_NAME)!.validate(data.ACTION, data.ARGS)

        } catch (error) {
            return false
        }

    }
    public addApplication(application: Application) {
        assert.ok(typeof application.getName === 'function', 'can not get name from action')
        assert.ok(typeof application.validate === 'function', 'action do not have a validate')
        this._applications.set(application.getName(), application)
    }

    public removeApplication(application: Application) {
        assert.ok(typeof application.getName === 'function', 'can not get name from action')
        this._applications.delete(application.getName())
    }

    public run(qr: string) {
        const { name, data }: ScanData<ApplicationProps> = this._parse(qr)
        assert.ok(data.APP_NAME && this._applications.has(data.APP_NAME))
        // 校验参数是否合法 
        this._applications.get(data.APP_NAME)!.validate(data.ACTION, data.ARGS)
        // 校验参数是否合法 
        this._applications.get(data.ACTION)!.run(qr)
    }
}