import { IAction } from "./interface/action"

export abstract class Action implements IAction {
    constructor(private _name: string) {
    }
    public abstract validate(..._args: string[]): boolean
    public abstract run(..._args: string[]): void
    public getName() {
        return this._name
    }
}