import assert from "assert";
import { ApplicationProps } from "./handler/ApplicationHandler"; 
import { IAction } from "./interface/action";
import { IApplication } from "./interface/application";


export abstract class Application implements IApplication {
    private _actions: Map<string, IAction> = new Map()
    constructor(protected _name: string, protected _address: string, protected _abi: string) {
    }
    public validate(action: string, args: string[]): boolean {
        try {
            assert.ok(this._actions.has(action))
            // 校验参数是否合法 
            return this._actions.get(action)!.validate(...args)
        } catch (error) {
            return false
        }

    }

    public getName() {
        return this._name
    }

    public addAction(action: IAction) {
        assert.ok(typeof action.getName === 'function', 'can not get name from action')
        assert.ok(typeof action.validate === 'function', 'action do not have a validate')
        this._actions.set(action.getName(), action)
    }

    public removeAction(action: IAction) {
        assert.ok(typeof action.getName === 'function', 'can not get name from action')
        this._actions.delete(action.getName())
    }

    public run(qr: string) {
        const data: ApplicationProps = JSON.parse(qr)
        this._actions.get(data.ACTION)!.run(...data.ARGS)
    }
}