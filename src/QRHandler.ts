import { IQRHandler } from "./interface/handler";

export abstract class QRHandler implements IQRHandler {
    public constructor(
        protected _name: string
    ) { }

    public getName() {
        return this._name
    }

    public abstract validate(_qr: string): boolean

    public abstract run(qr: string): void
}