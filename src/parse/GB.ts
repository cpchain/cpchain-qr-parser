import assert from "assert"
//去除字符串头部空格或指定字符  
const Trim = (value: string, c: string) => {
    return TrimStart(TrimEnd(value, c), c)
}
const TrimStart = (value: string, c: string) => {
    value = value?.replace(/^s*/, '');
    var rg = new RegExp("^" + c + "*");
    var str = value?.replace(rg, '');
    return str;
}

//去除字符串尾部空格或指定字符  
const TrimEnd = (value: string, c: string) => {
    var str = (value || '')?.trimEnd()
    var rg = new RegExp(c);
    var j = str.length;
    while (rg.test(str.charAt(--j))) {
        str = str.slice(0, j)
    } 
    return str
}



const line2Props = (text: string): [string, string | Array<string>] => {
    assert.ok(typeof text === 'string')
    const [propName, propValue] = text.split(':')
    /**
     * 数组处理
     */
    if (propValue?.startsWith('"')) {
        return [propName, Trim(propValue, '"').split(',')]
    } else {
        return [propName, Trim(propValue, '"')]
    }
}


const props2Line = (key: string, value: string | Array<string>): string => {
    assert.ok(typeof key === 'string')
    if (Array.isArray(value)) {
        return `${key}:"${value.join(',')}"`
    } else {
        return `${key}:${value}`
    }
}


export class GB {

    /**
     * 二维码gb解析传入的text
     * @param text gb文本
     */
    static parse<T>(text: string): { name: string, data: T } {
        assert(typeof text === 'string')
        const textLines = text.split("\n")
        assert(textLines.length > 0)
        const [name] = line2Props(textLines.shift()!)
        var object: any = {}
        textLines.forEach(lineText => {
            const [propName, value] = line2Props(lineText)
            if (propName !== 'END') {
                object[propName] = value
            }
        });
        return { name, data: object as T }
    }

    static stringify({ name, data }: { name: string, data: any }) {
        assert(typeof name === 'string')
        var result = `${name}:` + "\n"

        for (const key in data) {
            if (Object.prototype.hasOwnProperty.call(data, key)) {
                const element = data[key];
                result += props2Line(key, element) + "\n"
            }
        }
        result += 'END'
        console.log(result)
        return result
    }
}

