export interface IAction {
    public getName(): string
    public validate(..._args: string[]): boolean
    public run(..._args: string[]):void
}