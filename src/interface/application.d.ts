import assert from "assert";
import { ApplicationProps } from "./handler/ApplicationHandler";
import { QRHandler } from "./QRHandler";
import { IAction } from "./typings/action";


export interface IApplication {

    public validate(action: string, args: string[]): boolean

    public getName(): string

    public addAction(action: IAction)

    public removeAction(handler: QRHandler)

    public run(qr: string)
}