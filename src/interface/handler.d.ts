
export interface IQRHandler {
    public getName(): string
    public validate(_qr: string): boolean
    public run(qr: string)
}