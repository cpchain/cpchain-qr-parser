
import assert from "assert";
import { IQRHandler } from "./interface/handler";

export class QRParser {
    private _handlers: Map<string, IQRHandler> = new Map()

    public register(handler: IQRHandler) {
        assert.ok(typeof handler.getName === 'function', 'can not get name from handler')
        assert.ok(typeof handler.validate === 'function', 'handler do not have a validate')
        this._handlers.set(handler.getName(), handler)
    }

    public unregister(handler: IQRHandler) {
        assert.ok(typeof handler.getName === 'function', 'can not get name from handler')
        this._handlers.delete(handler.getName())
    }

    public getParserHandler(qr: string): IQRHandler | undefined {
        var result: IQRHandler
        this._handlers.forEach((handler, _name) => {
            if (handler.validate(qr)) {
                result = handler
                return
            }

        })
        return result!
    }

    public getHandlers(): Map<string, IQRHandler> {
        return this._handlers
    }
}